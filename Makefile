CC  = gcc
CXX = clang++

INCLUDES = 

CFLAGS   = -g -Wall -Werror $(INCLUDES)
CXXFLAGS = -g -Wall -Werror $(INCLUDES) -std=c++11

LDFLAGS = -pthread
LDLIBS  =

executables = http-server
objects = http-server.o

.PHONY: default
default: $(executables)
	
$(objects): 

.PHONY: clean
clean:
	rm -f *~ a.out core $(objects) $(executables)

.PHONY: all
all: clean default
